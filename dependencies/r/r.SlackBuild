#!/bin/bash

# Slackware build script for R

# Copyright 2022, 2023 Skaendo <skaendo@mailfence.com>
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

cd $(dirname $0) ; CWD=$(pwd)

PRGNAM=r
SRCNAM=R
VERSION=${VERSION:-4.2.2}
BUILD=${BUILD:-1}
TAG=${TAG:-_endo}
PKGTYPE=${PKGTYPE:-tgz}

if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) ARCH=i586 ;;
    arm*) ARCH=arm ;;
       *) ARCH=$( uname -m ) ;;
  esac
fi

if [ ! -z "${PRINT_PACKAGE_NAME}" ]; then
  echo "$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.$PKGTYPE"
  exit 0
fi

TMP=${TMP:-/tmp/endo}
PKG=$TMP/package-$PRGNAM
OUTPUT=${OUTPUT:-/tmp}

if [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "i686" ]; then
  SLKCFLAGS="-O2 -march=i686 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

set -e

rm -rf $PKG
mkdir -p $TMP $PKG $OUTPUT
cd $TMP
rm -rf $SRCNAM-$VERSION
tar xvf $CWD/$SRCNAM-$VERSION.tar.gz
cd $SRCNAM-$VERSION
chown -R root:root .
find -L . \
 \( -perm 777 -o -perm 775 -o -perm 750 -o -perm 711 -o -perm 555 \
  -o -perm 511 \) -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 640 -o -perm 600 -o -perm 444 \
  -o -perm 440 -o -perm 400 \) -exec chmod 644 {} \;

export JAVA_HOME=/usr/lib64/java

CFLAGS="$SLKCFLAGS -ffat-lto-objects" \
CXXFLAGS="$SLKCFLAGS -ffat-lto-objects" \
./configure \
  --prefix=/usr \
  --libexecdir=/usr/libexec/$SRCNAM \
  --sysconfdir=/etc/$SRCNAM \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --infodir=/usr/info \
  --mandir=/usr/man \
  rsharedir=/usr/share/$SRCNAM/ \
  rincludedir=/usr/include/$SRCNAM/ \
  rdocdir=/usr/doc/$PRGNAM-$VERSION \
  --enable-memory-profiling \
  --enable-R-shlib \
  --with-blas \
  --with-lapack \
  --with-x \
  F77=gfortran \
  LIBnn=lib${LIBDIRSUFFIX}
make
make pdf info
make install install-pdf install-info DESTDIR=$PKG

# install libRmath.so
cd src/nmath/standalone/
make shared
make install DESTDIR=$PKG

cd -

# Fixup R wrapper scripts.
sed -i "s|$PKG ||" $PKG/usr/bin/R
rm $PKG/usr/lib${LIBDIRSUFFIX}/R/bin/R
cd $PKG/usr/lib${LIBDIRSUFFIX}/R/bin
ln -s ../../../bin/R

# install some freedesktop.org compatibility
install -Dm644 $CWD/files/r.desktop -t $PKG/usr/share/applications
install -Dm644 $CWD/files/r.png -t $PKG/usr/share/pixmaps

# move the config directory to /etc and create symlinks
install -d $PKG/etc/R
cd $PKG/usr/lib${LIBDIRSUFFIX}/R/etc
for _i in *; do
  mv -f $_i $PKG/etc/R
  ln -s /etc/R/$_i $_i
done

# Install ld.so.conf.d file to ensure other applications access the shared lib
install -Dm644 $CWD/files/R.conf -t $PKG/etc/ld.so.conf.d

cd $TMP/$SRCNAM-$VERSION

find $PKG -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true

find $PKG/usr/man -type f -exec gzip -9 {} \;
for i in $( find $PKG/usr/man -type l ) ; do ln -s $( readlink $i ).gz $i.gz ; rm $i ; done

rm -f $PKG/usr/info/dir
#gzip -9 $PKG/usr/info/*.info*

mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION
cp -a ChangeLog COPYING README VERSION \
  $PKG/usr/doc/$PRGNAM-$VERSION
cat $CWD/$PRGNAM.SlackBuild > $PKG/usr/doc/$PRGNAM-$VERSION/$PRGNAM.SlackBuild

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc

cd $PKG
/sbin/makepkg -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.$PKGTYPE
