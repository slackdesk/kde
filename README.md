# SlackDesk KDE

This repo is currently a work in progress.    

Everything will build, however many doinst.sh files need to be edited/created, empty slack-desc files filled out, missing "README" files created and possibly some "misplaced" dependencies. See TODO and CONTRIBUTING for more info if you would like to help.

## IMPORTANT

Packages in the directory "1st" are "stock" Slackware packages and should be built and installed/upgraded before building the KDE packages. AFAIK these packages are only required for KDE.

# 

Slackware® is a registered trademark of [Patrick Volkerding](http://www.slackware.com/)  
Linux® is a registered trademark of [Linus Torvalds](http://www.linuxmark.org/)  
KDE® is a registered trademark of [KDE e.V.](https://ev.kde.org/)
